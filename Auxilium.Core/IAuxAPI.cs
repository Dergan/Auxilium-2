﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Auxilium.Core
{
    public enum LoginErrorCode
    {
        Unknown = 0,
        Success = 1,
        Banned = 2,
        WrongLogin = 3
    }

    public enum RegisterErrorCode
    {
        Unknown = 0,
        Success = 1,
        NicknameInUse = 2,
        EmailInUse = 3,
        UsernameInUse = 4,
        FillInEverything = 5,
    }

    public enum SendMessageErrorCode
    {
        Unknown = 0,
        Success = 1,
        AntiSpam = 2,

    }

    public enum ChangeChannelErrorCode
    {
        Unknown = 0,
        Success = 1,
        PasswordRequired = 2,
        NoAccess = 3,

    }

    public interface IAuxAPI
    {
        //no authentication required
        LoginErrorCode Login(string Username, string Password);
        RegisterErrorCode Register(string Username, string Password, string Nickname, string Email);

        //must be logged in
        bool SendSuggestion(string Text);
        void GetChannelList();
        ChangeChannelErrorCode ChangeChannel(ulong ChannelId);

        /// <summary>
        /// Send the message to the current channel
        /// </summary>
        /// <param name="Message">The message to send</param>
        /// <returns>Was send successfully</returns>
        SendMessageErrorCode SendChannelMessage(string Message);

        string GetNews();
    }
}