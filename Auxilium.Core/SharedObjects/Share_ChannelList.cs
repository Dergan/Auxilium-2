﻿using ProtoBuf;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Auxilium.Core.SharedObjects
{
    [ProtoContract]
    public class Share_ChannelList
    {
        [ProtoMember(1)]
        public Share_ChannelList[] Channels { get; set; }

        [ProtoMember(2)]
        public ulong CurrentChannel { get; set; }

        public Share_ChannelList()
        {

        }

        public Share_ChannelList(Share_ChannelList[] channels)
        {
            this.Channels = channels;
        }

        public void Dispose()
        {
            this.Channels = null;
        }
    }
}