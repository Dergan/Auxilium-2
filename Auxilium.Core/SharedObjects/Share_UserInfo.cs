﻿using ProtoBuf;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Auxilium.Core.SharedObjects
{
    [ProtoContract]
    public class Share_UserInfo
    {
        [ProtoMember(1)]
        public string Name { get; set; }

        [ProtoMember(2)]
        public int Rank { get; set; }

        [ProtoMember(3)]
        public ulong ID { get; set; }

        public Share_UserInfo()
        {
        }

        public Share_UserInfo(string name, int rank, ulong id)
        {
            this.Name = name;
            this.Rank = rank;
            this.ID = id;
        }
    }

}
