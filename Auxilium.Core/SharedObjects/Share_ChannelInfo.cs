﻿using ProtoBuf;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Auxilium.Core.SharedObjects
{
    [ProtoContract]
    public class Share_ChannelInfo
    {
        [ProtoMember(1)]
        public string Name { get; set; }

        [ProtoMember(2)]
        public ulong ID { get; set; }

        [ProtoMember(3)]
        public Share_UserInfo[] Users { get; set; }

        public Share_ChannelInfo()
        {

        }

        public Share_ChannelInfo(string name, ulong id, Share_UserInfo[] users)
        {
            this.Name = name;
            this.ID = id;
            this.Users = users;
        }
    }
}
