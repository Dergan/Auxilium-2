﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Auxilium.Core
{
    public class UserMessage
    {
        public DateTime WrittenAt { get; private set; }
        public string Message { get; private set; }
        public ulong UserId { get; private set; }
        public string Nickname { get; private set; }

        public UserMessage(DateTime WrittenAt, string Message, ulong UserId, string Nickname)
        {
            this.WrittenAt = WrittenAt;
            this.Message = Message;
            this.UserId = UserId;
            this.Nickname = Nickname;
        }

        public UserMessage()
        {

        }
    }
}
