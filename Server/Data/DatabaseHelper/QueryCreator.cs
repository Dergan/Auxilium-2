﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Auxilium_Server.Data.DatabaseHelper
{
    public class QueryCreator
    {
        public QueryCreator()
        {

        }

        /// <summary>
        /// Create a insert query
        /// </summary>
        /// <param name="Table">The target table</param>
        /// <param name="values">The values, Key=Colmn, Value = new Value</param>
        /// <returns>The query</returns>
        public MySqlCommand InsertQuery(string Table, ATable TableValues, bool ExcludePrimaryKey = true)
        {
            return InsertQuery(Table, new ATable[] { TableValues }, ExcludePrimaryKey);
        }

        /// <summary>
        /// Create a insert query
        /// </summary>
        /// <param name="Table">The target table</param>
        /// <param name="values">The values, Key=Colmn, Value = new Value</param>
        /// <returns>The query</returns>
        public MySqlCommand InsertQuery(string Table, ATable[] TableValues, bool ExcludePrimaryKey = true)
        {
            if (TableValues.Length == 0)
                return null;

            SortedList<int, List<Dictionary<string, object>>> values = new SortedList<int, List<Dictionary<string, object>>>();
            for (int j = 0; j < TableValues.Length; j++)
            {
                //30 is used for how many queries should be executed at once, 
                //Benchmark: 950-134s, 450-40s, 150-30s, 50-15s, 10-15s, 1-25s
                //somewhere between 10-50 is the fastest
                int index = j / 30;
                if (!values.ContainsKey(index))
                    values.Add(index, new List<Dictionary<string, object>>());
                values[index].Add(TableValues[j].GetFieldArray(ExcludePrimaryKey));
            }

            StringBuilder SB = new StringBuilder();
            int temp_counter = 0;
            MySqlCommand cmd = new MySqlCommand();

            foreach (List<Dictionary<string, object>> DictValue in values.Values)
            {
                int i = 0;
                SB.Append("INSERT INTO " + Table + " (");
                foreach (KeyValuePair<string, object> pair in DictValue[0])
                {
                    SB.Append(pair.Key);

                    if (i + 1 < DictValue[0].Count)
                        SB.Append(", ");
                    i++;
                }
                SB.Append(") VALUES(");
                i = 0;

                for (int j = 0; j < DictValue.Count; j++)
                {
                    foreach (KeyValuePair<string, object> pair in DictValue[j])
                    {
                        cmd.Parameters.AddWithValue("@value" + temp_counter, pair.Value);
                        SB.Append("@value" + temp_counter);
                        temp_counter++;

                        if (i + 1 < DictValue[j].Count)
                            SB.Append(", ");
                        else
                            SB.Append(")");
                        i++;
                    }

                    i = 0;
                    if (j + 1 < DictValue.Count)
                    {
                        SB.Append(", (");
                    }
                }
                SB.Append(";");
            }
            cmd.CommandText = SB.ToString();
            return cmd;
        }

        /// <summary>
        /// Create a update query
        /// </summary>
        /// <param name="Table">The target table</param>
        /// <param name="Values">The values to update, key=column, value=new value</param>
        /// <param name="Id">Where Id=</param>
        /// <returns>The query</returns>
        public MySqlCommand UpdateQuery(string Table, Dictionary<string, object> Values, ulong Id)
        {
            MySqlCommand cmd = new MySqlCommand();
            int temp_counter = 0;
            string Query = "UPDATE " + Table + " SET ";
            int i = 0;
            foreach (KeyValuePair<string, object> pair in Values)
            {
                cmd.Parameters.AddWithValue("@value" + temp_counter, pair.Value);
                Query += pair.Key + "=" + ("@value" + temp_counter);
                temp_counter++;

                if (i + 1 < Values.Count)
                    Query += ", ";
                i++;
            }
            Query += " WHERE Id=" + Id;
            cmd.CommandText = Query;
            return cmd;
        }

        /// <summary>
        /// Deltete a record
        /// </summary>
        /// <param name="Table">The target table</param>
        /// <param name="WhereName">Where Name = </param>
        /// <param name="Ids">The Ids to delete</param>
        /// <returns>The query</returns>
        public string DeleteQuery(string Table, string WhereName, int[] Ids)
        {
            string Query = "DELETE FROM " + Table + " WHERE " + WhereName + " IN (";

            for (int i = 0; i < Ids.Length; i++)
            {
                Query += Ids[i].ToString();

                if (i + 1 < Ids.Length)
                    Query += ", ";
            }
            Query += ")";
            return Query;
        }
    }
}
