﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Auxilium_Server.Data.DatabaseHelper
{
    public class DataParser
    {
        public DataParser()
        {

        }

        public static string Parse(object value)
        {
            if (value == null)
                return "NULL";

            Type valueType = value.GetType();
            if (valueType == typeof(byte) ||
                valueType == typeof(short) || valueType == typeof(ushort) ||
                valueType == typeof(int) || valueType == typeof(uint) ||
                valueType == typeof(long) || valueType == typeof(ulong) ||
                valueType == typeof(decimal))
            {
                return value.ToString();
            }
            else if (valueType == typeof(string))
            {
                return "'" + value.ToString().Replace("'", "''") + "'";
            }
            else if (valueType == typeof(DBNull))
            {
                return "NULL";
            }
            else if (valueType == typeof(DateTime))
            {
                DateTime time = (DateTime)value;
                return "'" + time.Year.ToString("D2") + "-" + time.Month.ToString("D2") + "-" + time.Day.ToString("D2") + " " + time.Hour.ToString("D2") + ":" + time.Minute.ToString("D2") + ":" + time.Second.ToString("D2") + "'";
            }
            else if (valueType == typeof(TimeSpan))
            {
                TimeSpan time = (TimeSpan)value;
                return "'" + time.Hours.ToString("D2") + ":" + time.Minutes.ToString("D2") + ":" + time.Seconds.ToString("D2") + "'";
            }
            else if (valueType == typeof(bool))
            {
                return (bool)value == true ? "1" : "0";
            }
            else
            {

            }
            return value.ToString();
        }
    }
}
