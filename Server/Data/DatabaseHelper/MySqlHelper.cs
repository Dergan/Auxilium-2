﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Auxilium_Server.Data.DatabaseHelper
{
    public class MySqlHelper
    {
        private MySqlConnection Connection;

        public MySqlHelper(MySqlConnection Connection)
        {
            this.Connection = Connection;
        }

        public T[] GetRecords<T>(string Query, params MySqlParameter[] Parameters)
        {
            List<T> temp = new List<T>();

            using (MySqlCommand cmd = new MySqlCommand(Query, Connection))
            {
                if (Parameters != null)
                {
                    for (int i = 0; i < Parameters.Length; i++)
                    {
                        cmd.Parameters.AddWithValue(Parameters[i].ParameterName, Parameters[i].Value);
                    }
                }

                using (MySqlDataReader reader = cmd.ExecuteReader())
                {
                    while (reader.Read())
                    {
                        object obj = Activator.CreateInstance(typeof(T));

                        if (obj as ATable != null)
                        {
                            (obj as ATable).ReadValues(reader, Connection);
                            temp.Add((T)obj);
                        }
                    }
                }
            }
            return temp.ToArray();
        }


        public int ExecuteQuery(string Query, params MySqlParameter[] Parameters)
        {
            using (MySqlCommand cmd = new MySqlCommand(Query, Connection))
            {
                if (Parameters != null)
                {
                    for (int i = 0; i < Parameters.Length; i++)
                    {
                        cmd.Parameters.AddWithValue(Parameters[i].ParameterName, Parameters[i].Value);
                    }
                }

                return cmd.ExecuteNonQuery();
            }
        }

        public void SimpleInsert(ATable record)
        {
            using (MySqlCommand cmd = record.GetInsertQuery(record.TableName))
            {
                cmd.Connection = this.Connection;
                cmd.ExecuteNonQuery();
            }
        }

        public void UpdateRecord(ATable record)
        {
            using (MySqlCommand cmd = record.GetUpdateQuery(record.TableName))
            {
                cmd.Connection = this.Connection;
                cmd.ExecuteNonQuery();
            }
        }

        public T InsertFetch<T>(ATable record)
        {
            using (MySqlCommand cmd = record.GetInsertQuery(record.TableName))
            {
                cmd.Connection = this.Connection;
                cmd.ExecuteNonQuery();

                if (cmd.LastInsertedId > 0)
                {
                    return GetRecords<T>(String.Format("SELECT * FROM {0} WHERE Id={1} LIMIT 1", record.TableName, cmd.LastInsertedId)).FirstOrDefault();
                }
            }
            return default(T);
        }

        public void InsertOrUpdate<T>(ATable record)
        {
            bool IsNew = record.GetPrimaryIdValue() == 0;

            if (IsNew)
            {
                InsertFetch<T>(record);
            }
            else
            {
                UpdateRecord(record);
            }
        }
    }
}
