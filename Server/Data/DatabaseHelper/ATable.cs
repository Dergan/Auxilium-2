﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;

namespace Auxilium_Server.Data.DatabaseHelper
{
    public abstract class ATable
    {
        internal MySqlConnection Connection;
        private string _tableName;

        public string TableName
        {
            get
            {
                if (_tableName != null)
                    return _tableName;

                _tableName = this.GetType().Name;
                return _tableName;
            }
        }

        public ATable()
        {

        }

        public void ReadValues(MySqlDataReader reader, MySqlConnection connection)
        {
            FieldInfo[] fields = this.GetType().GetFields();

            for (int i = 0; i < reader.VisibleFieldCount; i++)
            {
                string ColName = reader.GetName(i);
                string FieldColName = reader.GetName(i);

                int beginIndex = -1;
                int endIndex = -1;
                if ((beginIndex = ColName.IndexOf('(')) > 0 && (endIndex = ColName.LastIndexOf(')')) > 0)
                {
                    FieldColName = FieldColName.Substring(beginIndex + 1, FieldColName.Length - beginIndex - 2);
                }

                foreach (FieldInfo field in fields)
                {
                    if (field.Name != FieldColName)
                        continue;

                    if (field.GetCustomAttributes(typeof(TableColumnAttribute), false).Length == 0 &&
                        field.GetCustomAttributes(typeof(JoinColumnAttribute), false).Length == 0)
                    {
                        continue;
                    }

                    object obj = reader[ColName];
                    if (obj.GetType() != typeof(DBNull))
                    {
                        if (field.FieldType == typeof(bool))
                        {
                            int Value = 0;
                            if (int.TryParse(obj.ToString(), out Value))
                            {
                                field.SetValue(this, Value > 0);
                            }
                        }
                        else
                        {
                            field.SetValue(this, obj);
                        }
                    }
                }
            }

            this.Connection = connection;
        }

        public string[] Columns
        {
            get
            {
                FieldInfo[] fields = this.GetType().GetFields();
                List<string> cols = new List<string>();
                for (int j = 0; j < fields.Length; j++)
                {
                    if (fields[j].GetCustomAttributes(typeof(TableColumnAttribute), false).Length > 0)
                    {
                        cols.Add(fields[j].Name);
                    }
                }
                return cols.ToArray();
            }
        }

        public object[] GetValues(MySqlDataReader reader)
        {
            List<object> objects = new List<object>();
            foreach (FieldInfo field in this.GetType().GetFields())
                objects.Add(reader[field.Name]);
            return objects.ToArray();
        }

        public Dictionary<string, object> GetFieldArray(bool ExcludePrimaryKey)
        {
            Dictionary<string, object> values = new Dictionary<string, object>();

            foreach (FieldInfo field in this.GetType().GetFields())
            {
                if (ExcludePrimaryKey)
                {
                    if (field.GetCustomAttributes(typeof(PrimaryKeyAttribute), true).Length > 0)
                        continue;
                }

                if (field.GetCustomAttributes(typeof(TableColumnAttribute), true).Length > 0)
                    values.Add(field.Name, field.GetValue(this));
            }
            return values;
        }

        public string GetPrimaryKeyName()
        {
            foreach (FieldInfo info in this.GetType().GetFields())
            {
                if (info.GetCustomAttributes(typeof(PrimaryKeyAttribute), true).Length > 0)
                    return info.Name;
            }
            return null;
        }

        public ulong GetPrimaryIdValue()
        {
            foreach (FieldInfo info in this.GetType().GetFields())
            {
                if (info.GetCustomAttributes(typeof(PrimaryKeyAttribute), true).Length > 0)
                {
                    return (ulong)info.GetValue(this);
                }
            }
            return 0;
        }

        public MySqlCommand GetInsertQuery(string TableName, bool ExcludePrimaryKey = true)
        {
            return new QueryCreator().InsertQuery(TableName, this, ExcludePrimaryKey);
        }

        public MySqlCommand GetUpdateQuery(string TableName, bool ExcludePrimaryKey = true)
        {
            return new QueryCreator().UpdateQuery(TableName, GetFieldArray(true), GetPrimaryIdValue());
        }
    }
}
