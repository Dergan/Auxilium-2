﻿using Auxilium_Server.Data.DatabaseHelper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Auxilium_Server.Data.Tables
{
    public class tblChannel : ATable
    {
        public tblChannel()
            : base()
        {

        }

        [PrimaryKey]
        [TableColumn]
        public ulong Id;

        [TableColumn]
        public string Name;
    }
}