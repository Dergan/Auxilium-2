﻿using Auxilium_Server.Data.DatabaseHelper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Auxilium_Server.Data.Tables
{
    public class tblUser : ATable
    {
        public tblUser()
            : base()
        {

        }

        [PrimaryKey]
        [TableColumn]
        public ulong Id;

        [TableColumn]
        public string Username;

        [TableColumn]
        public string Password;

        [TableColumn]
        public string Email;

        [TableColumn]
        public int Points;

        [TableColumn]
        public int Rank;

        [TableColumn]
        public string Bio;

        [TableColumn]
        public string Avatar;

        [TableColumn]
        public string Forum;

        [TableColumn]
        public string Nickname;
    }
}
