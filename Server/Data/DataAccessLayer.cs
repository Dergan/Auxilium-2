﻿using Auxilium_Server.Data.Tables;
using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Auxilium_Server.Data
{
    public class DataAccessLayer : IDisposable
    {
        private MySqlConnection connection;

        public DatabaseHelper.MySqlHelper SqlHelper
        {
            get; private set;
        }

        public DataAccessLayer()
        {


            this.connection = new MySqlConnection("server=192.168.3.27;Port=3306;database=auxilium;user id=aux;password=123456");
            this.connection.Open();
            this.SqlHelper = new DatabaseHelper.MySqlHelper(this.connection);
        }

        public tblUser GetUser(ulong Id)
        {
            return SqlHelper.GetRecords<tblUser>("SELECT * FROM tblUser WHERE Id=@Value LIMIT 1", new MySqlParameter("@Value", Id)).FirstOrDefault();
        }

        public tblUser GetUser(string Username)
        {
            return SqlHelper.GetRecords<tblUser>("SELECT * FROM tblUser WHERE Username=@Value LIMIT 1", new MySqlParameter("@Value", Username.ToLower())).FirstOrDefault();
        }

        public tblUser GetUserByEmail(string Email)
        {
            return SqlHelper.GetRecords<tblUser>("SELECT * FROM tblUser WHERE Email=@Value LIMIT 1", new MySqlParameter("@Value", Email.ToLower())).FirstOrDefault();
        }

        public tblUser GetUser(string Username, string Password)
        {
            return SqlHelper.GetRecords<tblUser>("SELECT * FROM tblUser WHERE (Username=@UserValue OR Email=@UserValue) AND Password=@PassValue LIMIT 1", new MySqlParameter("@UserValue", Username.ToLower()),
                                                                                                                                                          new MySqlParameter("@PassValue", Password)).FirstOrDefault();
        }

        public tblUser[] GetUserFromPartialName(string partialName)
        {
            return SqlHelper.GetRecords<tblUser>("SELECT * FROM tblUser WHERE Username LIKE '%@UserValue%'", new MySqlParameter("@UserValue", partialName)).ToArray();
        }

        public bool InsertUser(string username, string password, string email)
        {
            if (GetUser(username) != null || GetUserByEmail(email) != null)
                return false;

            tblUser user = new tblUser();
            user.Avatar = "";
            user.Bio = "";
            user.Email = email;
            user.Forum = "";
            user.Nickname = "";
            user.Password = password;
            user.Points = 0;
            user.Rank = 0;
            user.Username = username;

            SqlHelper.SimpleInsert(user);
            return true;
        }

        public tblChannel[] GetChannels()
        {
            return SqlHelper.GetRecords<tblChannel>("SELECT * FROM tblChannel").ToArray();
        }

        public bool UpdateRank(ulong UserId, int points, int rank)
        {
            tblUser user = GetUser(UserId);

            if (user == null)
                return false;

            user.Rank = rank;
            user.Points = points;
            SqlHelper.UpdateRecord(user);

            return true;
        }

        public void AddNewSuggestion(ulong UserId, string text)
        {
            tblSuggestions suggestion = new tblSuggestions();
            suggestion.Suggestion = text;
            suggestion.UserId = UserId;
            SqlHelper.SimpleInsert(suggestion);
        }

        /*public bool AddPrivateMessage(int fromId, int toId, string subject, string message)
        {
            PrivateMessageInfo pm = new PrivateMessageInfo()
            {
                FromID = fromId,
                ToID = toId,
                Subject = subject,
                TimeSent = DateTime.UtcNow,
                Message = message,
                TimeRead = DateTime.FromOADate(0)
            };

            DatabaseDataContext db = new DatabaseDataContext(ConnectionString);

            db.PrivateMessageInfos.InsertOnSubmit(pm);
            db.SubmitChanges();

            return db.PrivateMessageInfos.Contains(pm);
        }

        public PrivateMessageInfo[] GetPrivateMessages(string username)
        {
            User user = GetUser(username);
            return GetPrivateMessages(user.Id);
        }

        public PrivateMessageInfo[] GetPrivateMessages(int userId)
        {
            DatabaseDataContext db = new DatabaseDataContext(ConnectionString);

            return db.PrivateMessageInfos.Where(x =>
                x.FromID == userId ||
                x.ToID == userId)
                .ToArray();
        }*/

        public void Dispose()
        {
            this.connection.Close();
        }
    }
}