﻿using Auxilium.Core;
using Auxilium.Core.SharedObjects;
using Auxilium_Server.Data;
using Auxilium_Server.Data.Tables;
using LiteCode.Attributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Auxilium_Server.src
{
    public class PeerAuxAPI : IAuxAPI
    {
        public UserState State { get; private set; }

        public void GetChannelList()
        {
            this.State = new UserState();
        }


        [NoException()]
        [RemoteExecution(30000, ChangeChannelErrorCode.NoAccess)]
        public ChangeChannelErrorCode ChangeChannel(ulong ChannelId)
        {
            State.Channel = ChannelId;
            return ChangeChannelErrorCode.NoAccess;
        }

        [NoException()]
        [RemoteExecution(30000, "")]
        public string GetNews()
        {
            return "Some news";
        }

        [NoException()]
        [RemoteExecution(30000, LoginErrorCode.WrongLogin)]
        public LoginErrorCode Login(string Username, string Password)
        {
            using (DataAccessLayer dal = new DataAccessLayer())
            {
                tblUser user = dal.GetUser(Username, Password);

                if (user != null)
                {
                    State.Authenticated = true;
                    State.ID = user.Id;
                    State.Channel = 1;
                    State.Points = user.Points;
                    State.Rank = user.Rank;
                    State.Username = user.Username;

                    //broadcast the a user joined the channel

                    return LoginErrorCode.Success;
                }

                return LoginErrorCode.WrongLogin;
            }

        }

        [NoException()]
        [RemoteExecution(30000, RegisterErrorCode.UsernameInUse)]
        public RegisterErrorCode Register(string Username, string Password, string Nickname, string Email)
        {
            if (String.IsNullOrWhiteSpace(Username) || String.IsNullOrWhiteSpace(Password) ||
                String.IsNullOrWhiteSpace(Email))
            {
                return RegisterErrorCode.FillInEverything;
            }

            using (DataAccessLayer dal = new DataAccessLayer())
            {
                bool success = dal.InsertUser(Username, Password, Email);

                if (success)
                {
                    return RegisterErrorCode.Success;
                }

                return RegisterErrorCode.UsernameInUse;
            }
        }

        [NoException()]
        [RemoteExecution(30000, SendMessageErrorCode.AntiSpam)]
        public SendMessageErrorCode SendChannelMessage(string Message)
        {
            State.AddPoints(5); //AWARD 5 POINTS FOR ACTIVITY***

            //broadcast to every user in the channel the message
            /*BroadcastMessage message = new BroadcastMessage(packet.Message, client.Value.Username, client.Value.ID);
            foreach (Client c in _server.Clients)
            {
                if (c != client && c.Value.Channel == client.Value.Channel)
                {
                    message.Execute(c);
                }
            }
            AddMessageToRecent(message, client.Value.Channel);*/

            return SendMessageErrorCode.Success;
        }

        [NoException()]
        [RemoteExecution(30000, false)]
        public bool SendSuggestion(string Text)
        {
            using (DataAccessLayer dal = new DataAccessLayer())
            {
                tblUser user = dal.GetUser(State.Username);
                if (user != null)
                {
                    dal.AddNewSuggestion(user.Id, Text);
                    return true;
                }
            }
            return false;
        }

        [NoException()]
        [RemoteExecution(30000, null)]
        public Share_ChannelInfo[] GetChannels()
        {
            using (DataAccessLayer dal = new DataAccessLayer())
            {
                tblChannel[] channels = dal.GetChannels();
                List<Share_ChannelInfo> channelInfos = new List<Share_ChannelInfo>();
                foreach (tblChannel channel in channels)
                    channelInfos.Add(new Share_ChannelInfo(channel.Name, channel.Id, Program.GetUsersFromChannel(channel)));
                return channelInfos.ToArray();
            }
        }
    }
}
