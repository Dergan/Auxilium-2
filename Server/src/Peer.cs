﻿using LiteCode;
using SecureSocketProtocol3;
using SecureSocketProtocol3.Network;
using SecureSocketProtocol3.Security.DataIntegrity;
using SecureSocketProtocol3.Security.Handshakes;
using SecureSocketProtocol3.Security.Layers;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

namespace Auxilium_Server.src
{
    public class Peer : SSPClient
    {
        public Peer()
            : base()
        {

        }


        public override void onConnect()
        {
            Console.WriteLine("[" + DateTime.Now.ToString("HH:mm:ss") + "] connected, Peer connected " + base.RemoteIp);
        }

        public override void onDisconnect(DisconnectReason Reason)
        {

        }

        public override void onException(Exception ex, ErrorType errorType)
        {
            Console.WriteLine(ex.Message);
        }

        public override void onBeforeConnect()
        {
            base.RegisterOperationalSocket(new LiteCodeClient(this));
        }

        public override void onOperationalSocket_Connected(OperationalSocket OPSocket)
        {

        }

        public override void onOperationalSocket_BeforeConnect(OperationalSocket OPSocket)
        {
            if (OPSocket as LiteCodeClient != null)
            {
                LiteCodeClient liteClient = OPSocket as LiteCodeClient;
                liteClient.ShareClass("AuxAPI", typeof(PeerAuxAPI), false, 1);
            }
        }

        public override void onOperationalSocket_Disconnected(OperationalSocket OPSocket, DisconnectReason Reason)
        {

        }

        public override void onApplyLayers(LayerSystem layerSystem)
        {
            layerSystem.AddLayer(new QuickLzLayer());
            layerSystem.AddLayer(new AesLayer(base.Connection));
            layerSystem.AddLayer(new TwoFishLayer(base.Connection));
            layerSystem.AddLayer(new RC4Layer());
            layerSystem.AddLayer(new XmlHidingLayer()); //calmdown TLA, we're only sending pet names and drinking thee, we're all friends here
        }

        private IDataIntegrityLayer _dataIntegrityLayer;
        public override IDataIntegrityLayer DataIntegrityLayer
        {
            get
            {
                if (_dataIntegrityLayer == null)
                    _dataIntegrityLayer = new HMacLayer(this);
                return _dataIntegrityLayer;
            }
        }

        public override void onApplyHandshakes(HandshakeSystem handshakeSystem)
        {
            //to generate a new key...
            //RSAEncryption GenPrivateKeyc = new RSAEncryption(4096, true);
            //GenPrivateKeyc.GeneratePrivateKey();
            //string PreGenPrivateKey = GenPrivateKeyc.PrivateKey;
            
            //this is only a sample RSA-Private-Key, generate your own with the code above
            //do never share the private-key with anyone
            //when running your own auxilium chat server, generate your own first
            string PrePrivKey = @"<RSAKeyValue>
                                    <Modulus>ui7rEnh9ovwwe1c8CRthTOqnjB9EjoPgeiBL1291sXpOf0XPhHP+melBzr0yx2rSgTvgGboSFUjNQn6cxGwWklObZHs+kWev1uz2QgDC/j41es9bmLtH2P+AEFAXb1DhYSiLA/USaRyQ+LZqPdpb/euWofXuIcb2y+R8vtVVFkU=</Modulus>
                                    <Exponent>AQAB</Exponent>
                                    <P>zWiN6Zu6BfYrIgnYNyO5JTSEWfM689ovwL/agFW8uadmJOzeCeprrjGf8kyjcxVljw2om9Vw/H4ZcN0HR7YShw==</P>
                                    <Q>6AotVwrrC0E/+x5KOpp1fsl1zTHBdIMzsbwds1Ym1L3JHlstkNP8DejfxH9Ej2sYttLogJ5g5Ra781d2WxTn0w==</Q>
                                    <DP>InhnSEq/3vw+pMmuJSKzkVDM3SN6Qy3cUaZgjqTUtPsoow20/Uj/pQ3i35CI5Wkzz9vk7bHV8ilfL5eH/zrIxQ==</DP>
                                    <DQ>GJ4jy01MPIhyqki/ZVJHzui+x8NUm/DjhiLIH+OvAPkVolPYFLp4zlz7iJRcCL87AwKDSkoDS6rKy/lmhClGow==</DQ>
                                    <InverseQ>BSTKfqQr8WqrvOGWqmK8PupDfvI9d7u8UYJEpsAfEJFoSpTpG/WmSePBut0ukki3/qOeaOqULItu5XNnthtkPw==</InverseQ>
                                    <D>I8CowYZD0g2NndHVpIYOfD+/ZugGOTvX2nvjNH6h4i/zbPtR60R/Cr1BNtscKjE4NTrzQN17ZXzydadsoUeEWccVVdH7pJXCUCm6klLbgKZDrEAhJInDnkkzFRI4EMGLKeW1o41s/kqw1XF4zVFDRFLlEH1CwJ7y3ewqc+KppM0=</D>
                                  </RSAKeyValue>";

            handshakeSystem.AddLayer(new SimpleRsaHandshake(this, PrePrivKey));
        }
    }
}
