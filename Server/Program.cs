﻿using System;
using System.Collections.Generic;
using System.Linq;
using Auxilium_Server.Data.Tables;
using Auxilium_Server.Data;
using Auxilium_Server.src;
using Auxilium.Core.SharedObjects;
using System.Diagnostics;
using Auxilium.Core;

namespace Auxilium_Server
{
    internal class Program
    {
        public static Server Server { get; private set; }
        public static DateTime LastBackup { get; private set; }
        public static System.Threading.Timer ChatMonitor { get; private set; }
        public static Dictionary<ulong, List<UserMessage>> RecentMessages { get; private set; }

        private static void Main(string[] args)
        {
            RecentMessages = new Dictionary<ulong, List<UserMessage>>();

            Console.WriteLine("Gathering channels...");
            using (DataAccessLayer dal = new DataAccessLayer())
            {
                tblChannel[] channels = dal.GetChannels();
                foreach (tblChannel channel in channels)
                    RecentMessages.Add(channel.Id, new List<UserMessage>());
            }

            Server = new src.Server();
            ChatMonitor = new System.Threading.Timer(Monitor, null, TimeSpan.FromSeconds(1), TimeSpan.FromMinutes(4));

            Console.WriteLine("Listening at port " + Server.serverProperties.ListenPort);
            Process.GetCurrentProcess().WaitForExit();
        }

        #region " Monitor "

        private static void Monitor(object state)
        {
            UpdateAndSaveUsers(false);
        }

        private static void AwardPoints(PeerAuxAPI client)
        {
            if ((DateTime.Now - client.State.LastAction).TotalMinutes >= 5)
            {
                client.State.Idle = true;
            }

            //2812000 at 4.65 points per second should require about 1 week of active time to max rank.
            double points = (DateTime.Now - client.State.LastPayout).TotalSeconds * 4.65;
            client.State.LastPayout = DateTime.Now;

            if (client.State.Idle)
            {
                if (client.State.Rank < 29)
                {
                    client.State.AddPoints((int)(points * 0.1f));
                }
            }
            else
            {
                client.State.AddPoints((int)points);
            }
        }

        private static void UpdateAndSaveUsers(bool shutDown)
        {
            bool doBackup = false;
            if ((DateTime.Now - LastBackup).TotalMinutes >= 2)
            {
                doBackup = true;
                LastBackup = DateTime.Now;
            }

            doBackup = true;

            /*foreach (Client c in Server.Clients)
            {
                if (!c.Value.Authenticated) continue;

                AwardPoints(c);

                if (!shutDown)
                {
                    SendChannels(Server.Clients);
                }

                if (doBackup || shutDown)
                {
                    using (DataAccessLayer dal = new DataAccessLayer())
                    {
                        tblUser user = dal.GetUser(c.Value.Username);
                        if (user != null)
                        {
                            dal.UpdateRank(user.Id, c.Value.Points, c.Value.Rank);
                        }
                    }
                }
            }*/
        }

        #endregion " Monitor "

        #region " Packet Handlers "
        
        /*private static void HandlePrivateMessagesRequestPacket(Client client)
        {
            PrivateMessageInfo[] pms = Accessor.GetPrivateMessages(client.Value.ID);

            foreach (PrivateMessageInfo pm in pms)
            {
                new PrivateMessage(pm.Subject,
                    Accessor.GetUser(pm.FromID).Username,
                    Accessor.GetUser(pm.ToID).Username,
                    pm.TimeSent,
                    pm.Message).Execute(client);
            }
        }*/
        

        #endregion " Packet Handlers "
        
        public static Share_UserInfo[] GetUsersFromChannel(tblChannel channel)
        {
            /*List<UserInfo> users = new List<UserInfo>();
            foreach (Client client in _server.Clients)
                if (client.Value.Channel == channel.Id)
                    users.Add(new UserInfo(client.Value.Username, client.Value.Rank, client.Value.ID));
            return users.ToArray();*/
            return new Share_UserInfo[0];
        }

        private static void AddMessageToRecent(PeerAuxAPI User, string Message)
        {
            var messages = RecentMessages.First(x => x.Key == User.State.Channel).Value;

            if (messages.Count == 10)
                messages.RemoveAt(0);

            messages.Add(new UserMessage(DateTime.Now, Message, User.State.ID, User.State.NickName));
        }
        
    }
}